let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseId")
let token = localStorage.getItem("token")


fetch(`https://warm-ravine-16344.herokuapp.com/api/courses/${courseId}/activate`, {
	method: "PATCH",
	headers: {
		"Authorization" : `Bearer ${token}`
	}
})
.then(res => {
	return res.json()})
.then(data => {
	if(data) {
		alert("Course Activated")
	} else {
		alert("Something went wrong")
	}	
})
